# probability
assignments for the UvA AI 2018 course 53141BPC3Y Basic Probability

## convert to pdf
install jupyter and texlive, then run:
```
$ jupyter nbconvert ./my_notebook.ipynb --to pdf
```
